# Kulendayz 2018: Pifkan

## Od backenda do frontenda uz AWS i Vue.js

* Damjan Namjesnik (frontend)
* Kornelije Sajler (backend)

## Frontend

* vuejs

## Backend

* serverless framework
* AWS Lambda
* AWS Dynamo
* AWS Step Functions
* python/nodejs
