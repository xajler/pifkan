import Vue from 'vue'
import '@mdi/font/css/materialdesignicons.css'

import {
  Vuetify,
  VApp,
  VBottomNav,
  VList,
  VBtn,
  VCard,
  VIcon,
  VGrid,
  VToolbar,
  VParallax,
  VTextField,
  VForm,
  VSnackbar,
  VDataTable,
  VExpansionPanel,
  transitions
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'mdi',
  theme: {
    'primary': '#2E7D32',
    'secondary': '#7CB342',
    'accent': '#E8F5E9',
    'error': '#F44336',
    'warning': '#ffeb3b',
    'info': '#2196F3',
    'success': '#4CAF50'
  },
  components: {
    VApp,
    VBottomNav,
    VList,
    VBtn,
    VCard,
    VIcon,
    VGrid,
    VToolbar,
    VParallax,
    VTextField,
    VForm,
    VSnackbar,
    VDataTable,
    VExpansionPanel,
    transitions
  }
})
