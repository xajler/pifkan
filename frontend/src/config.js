// usually you will get api-key from environment variable
export default {
  apiKey: 'vfJdiFCpOs3ouWyTljgaS9ZvtCww92jl3c0vFqv8',
  urls: {
    login: 'https://sj0alae9c7.execute-api.us-east-1.amazonaws.com/dev/api/users/auth',
    register: 'https://sj0alae9c7.execute-api.us-east-1.amazonaws.com/dev/api/users',
    getAllDraws: 'https://k60z54x599.execute-api.us-east-1.amazonaws.com/dev/api/draws',
    getAllCodes: 'https://naphp05qe5.execute-api.us-east-1.amazonaws.com/dev/api/codes',
    submitCode: 'https://9w0x66vzk8.execute-api.us-east-1.amazonaws.com/dev/api/draws/code',
    drawCode: 'https://9w0x66vzk8.execute-api.us-east-1.amazonaws.com/dev/api/draws/draw'
  }
}
