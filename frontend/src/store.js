import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import config from './config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    allCodes: [],
    draws: [],
    error: null,
    lastDraw: null,
    loading: false,
    user: null
  },
  getters: {
    loggedIn (state) {
      return state.user !== null
    }
  },
  mutations: {
    setCodes (state, allCodes) {
      state.allCodes = allCodes
    },
    setDraws (state, draws) {
      state.draws = draws
    },
    setError (state, error) {
      // state.error = error
      console.log(error)
    },
    setLoading (state, loading) {
      state.loading = loading
    },
    setLastDraw (state, lastDraw) {
      state.lastDraw = lastDraw
    },
    setUser (state, userData) {
      state.user = userData
      localStorage['userData'] = JSON.stringify(userData)
    }
  },
  actions: {
    async drawCode ({ state, commit, dispatch }) {
      commit('setError', null)
      commit('setLoading', true)
      let requestConfig = {
        headers: {
          'Authorization': `Bearer ${state.user.token}`
        },
        crossdomain: true,
        timeout: 5000,
        validateStatus: status => status < 500
      }
      let res
      try {
        res = await axios.post(config.urls.drawCode, {}, requestConfig)
      } catch (e) {
        commit('setError', e)
        commit('setLoading', false)
        return Promise.resolve(false)
      }
      // special case: nobody played!!!
      if (res && res.status === 404) {
        commit('setLastDraw', {
          'code': null,
          'submittedBy': 'NITKO NIJE IGRAO!',
          'submittedAt': null
        })
        commit('setLoading', false)
        return Promise.resolve(true)
      }
      commit('setLastDraw', res.data)
      commit('setLoading', false)
      return Promise.resolve(true)
    },
    async getAllCodes ({ state, commit, dispatch }) {
      commit('setError', null)
      commit('setLoading', true)
      let requestConfig = {
        headers: {
          'Authorization': `Bearer ${state.user.token}`
        },
        crossdomain: true,
        timeout: 5000
      }
      let res
      try {
        res = await axios.get(config.urls.getAllCodes, requestConfig)
      } catch (e) {
        commit('setError', e)
        commit('setLoading', false)
        return Promise.resolve(false)
      }
      commit('setCodes', res.data)
      commit('setLoading', false)
      return Promise.resolve(true)
    },
    async getAllDraws ({ state, commit, dispatch }) {
      commit('setError', null)
      commit('setLoading', true)
      let requestConfig = {
        headers: {
          'Authorization': `Bearer ${state.user.token}`
        },
        crossdomain: true,
        timeout: 5000
      }
      let res
      try {
        res = await axios.get(config.urls.getAllDraws, requestConfig)
      } catch (e) {
        commit('setError', e)
        commit('setLoading', false)
        return Promise.resolve(false)
      }
      commit('setDraws', res.data)
      commit('setLoading', false)
      return Promise.resolve(true)
    },
    async login ({ state, commit, dispatch }, credentials) {
      commit('setError', null)
      commit('setLoading', true)
      let requestConfig = {
        headers: {
          'x-api-key': config.apiKey
        },
        crossdomain: true,
        timeout: 5000
      }
      // try to register if needed, then login
      if (!localStorage['userData']) {
        try {
          await axios.post(config.urls.register, credentials, requestConfig)
        } catch (e) {
        }
      }
      let resAuth
      try {
        resAuth = await axios.post(config.urls.login, credentials, requestConfig)
      } catch (e) {
        commit('setError', e)
        commit('setLoading', false)
        return Promise.resolve(false)
      }
      if (resAuth && resAuth.status === 200) {
        commit('setUser', {
          email: credentials.email,
          role: resAuth.data.role,
          token: resAuth.data.refresh_token
        })
        commit('setLoading', false)
        return Promise.resolve(true)
      } else {
        commit('setError', 'ERROR')
        commit('setLoading', false)
        return Promise.resolve(false)
      }
    },
    logout ({ state, commit, dispatch }) {
      // TODO: axios.post...
      commit('setUser', null)
      delete localStorage['userData']
    },
    async submitCode ({ state, commit, dispatch }, code) {
      commit('setError', null)
      commit('setLoading', true)
      let requestConfig = {
        headers: {
          'Authorization': `Bearer ${state.user.token}`
        },
        crossdomain: true,
        timeout: 5000
      }
      let codeData = { code: code }
      let res
      try {
        res = await axios.post(config.urls.submitCode, codeData, requestConfig)
      } catch (e) {
        commit('setError', e)
        commit('setLoading', false)
        return Promise.resolve(false)
      }
      if (res && res.status === 201) {
        commit('setLoading', false)
        return Promise.resolve(true)
      } else {
        commit('setError', 'ERROR')
        commit('setLoading', false)
        return Promise.resolve(false)
      }
    },
    async tryLogin ({ state, commit, dispatch }) {
      if (localStorage['userData']) {
        let userData = JSON.parse(localStorage['userData'])
        console.log('Trying to log in with ' + userData.email)
        return dispatch('login', {
          email: userData.email
        })
      }
    }
  }
})
