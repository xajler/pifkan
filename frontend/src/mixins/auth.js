export default {
  computed: {
    isAdmin () {
      return this.loggedInUser && this.loggedInUser.role === 'admin'
    },
    loggedIn () {
      return this.$store.getters['loggedIn']
    },
    loggedInUser () {
      return this.$store.state.user
    }
  }
}
