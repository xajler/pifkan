const dateTimeFormatter = new Intl.DateTimeFormat('hr-HR', {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  second: 'numeric'
})

export default {
  filters: {
    date (val) {
      return dateTimeFormatter.format(new Date(val))
    }
  }
}
