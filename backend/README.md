# Backend

* serverless framework
* AWS Lambda
* AWS Dynamo
* AWS Step Functions
* python/nodejs

## References

[JWT verify for Cognito](https://aws.amazon.com/blogs/mobile/integrating-amazon-cognito-user-pools-with-api-gateway/)
[serverless CORS](https://serverless.com/blog/cors-api-gateway-survival-guide/)

