import json
import os
import boto3
from auth import validate_admin, validate_user
from botocore.exceptions import ClientError

dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(os.environ.get('DYNAMODB_TABLE'))


def get(event, context):
    event_data = json.loads(json.dumps(event))
    print(event_data['pathParameters'])
    auth_header = event_data['headers']['Authorization']
    code = None

    if event_data['pathParameters']:
        code = event_data['pathParameters']['code']
        print(code)
        return _create_response(_get_one(code, auth_header))
    else:
        print('no code')
        return _create_response(_get_all(auth_header))


def _get_one(code, auth_header):
    status, body = validate_user(auth_header)
    if status > 200: return _create_response((status, body))
    try:
        result = table.get_item(Key={'code': code})
        print(result)
        result['Item']
        return (200, {'success': 'Code {0} found'.format(code)})
    except KeyError:
        return (404, {'error': 'Code {0} is not found'.format(code)})
    except ClientError as e:
        print('Unable to retrive code')
        print(e)
        return (500, {'error': 'Unable to retrieve code, server error'})


def _get_all(auth_header):
    status, body = validate_admin(auth_header)
    if status > 200: return _create_response((status, body))
    try:
        result = table.scan()
        print(result)

        if 'Items' in result:
            codes = [i['code'] for i in result['Items']]
            print(codes)
            return (200, codes)
        else:
            return (200, [])
    except ClientError as e:
        print('Unable to retrive codes')
        print(e)
        return (500, {'error': 'Unable to retrieve codes, server error'})


def _create_response(response):
    print(response)
    status, body = response
    result = {
        'statusCode': status,
        'body': json.dumps(body),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
         }
    }
    print(result)
    return result
