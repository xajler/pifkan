import string
import random
import os
import boto3
from botocore.exceptions import ClientError


def _code_gen(size=7, chars=string.ascii_lowercase.upper() + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def generate_code(n):
    i = 0
    codes = []

    while i <= n:
        codes.append(_code_gen())
        i = i + 1

    # print(*codes, sep='\n')
    return codes


def store_codes(codes):
    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table(os.environ.get('DYNAMODB_TABLE'))
    try:
        with table.batch_writer() as batch:
            for code in codes:
                print(code)
                item = {'code': code}
                batch.put_item(Item=item)
    except ClientError as e:
        print(e)
        raise
