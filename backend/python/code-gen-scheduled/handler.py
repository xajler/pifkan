import datetime
from helper import generate_code, store_codes


def execute(event, context):
    codes = generate_code(100)
    store_codes(codes)
    current_date = datetime.datetime.now()
    print(current_date)

    return {
        'message': 'Generated 100 codes at: {0}'.format(current_date),
        'event': event
    }
