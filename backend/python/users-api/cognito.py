import boto3
import os
from botocore.exceptions import ClientError

client = boto3.client('cognito-idp')
generic_pass = "%xg123$Yn"
fst_time_pass = "Abc$1234"
user_pool_id = os.environ.get('COGNITO_POOL_ID')
client_id = os.environ.get('COGNITO_CLIENT_ID')


def auth(data):
    print(data['email'])
    try:
        response = client.admin_initiate_auth(
                UserPoolId=user_pool_id,
                ClientId=client_id,
                AuthFlow='ADMIN_NO_SRP_AUTH',
                AuthParameters={
                    'USERNAME': data['email'],
                    'PASSWORD': generic_pass
                }
        )
        print(response)
        id_token = response['AuthenticationResult']['IdToken']
        refresh_token = response['AuthenticationResult']['RefreshToken']
        grp_resp = client.admin_list_groups_for_user(
            UserPoolId=user_pool_id,
            Username=data['email'],
            Limit=1,
        )
        print(grp_resp)
        role = grp_resp['Groups'][0]['GroupName']
        return (200, {'role': role, 'refresh_token': refresh_token})
    except ClientError as e:
        print(e.response)
        if e.response['Error']['Code'] == 'UserNotFoundException':
            return (404, {'error': 'User does not exists'})
        return (500, {'error': 'Unable to authenticate user, server error'})


def register(data):
    print(data['email'])
    username = data['email']
    try:
        create_resp = client.admin_create_user(
                UserPoolId=user_pool_id,
                Username=username,
                TemporaryPassword=fst_time_pass,
                MessageAction='SUPPRESS',
                UserAttributes=[{
                    'Name': 'email',
                    'Value': username
                }]
        )
        print(create_resp)
        auth_resp = client.admin_initiate_auth(
                UserPoolId=user_pool_id,
                ClientId=client_id,
                AuthFlow='ADMIN_NO_SRP_AUTH',
                AuthParameters={
                    'USERNAME': username,
                    'PASSWORD': fst_time_pass
                }
        )
        print(auth_resp)
        response = client.respond_to_auth_challenge(
            ClientId=client_id,
            ChallengeName='NEW_PASSWORD_REQUIRED',
            Session=auth_resp['Session'],
            ChallengeResponses={
                'USERNAME': username,
                'NEW_PASSWORD': generic_pass
            }
        )
        print(response)
        _add_user_to_group(username)
        return (201, {})
    except ClientError as e:
        print(e.response)
        if e.response['Error']['Code'] == 'UsernameExistsException':
            return (409, {'error': 'Email is used, please try another one'})
        return (500, {'error': 'Unable to save user, server error'})


def _add_user_to_group(username):
    response = client.admin_add_user_to_group(
        UserPoolId=user_pool_id,
        Username=username,
        GroupName='user'
    )
    print(response)

