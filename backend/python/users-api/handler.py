import json
import cognito


def auth(event, context):
    print(event)
    event_data = json.loads(json.dumps(event))
    data = json.loads(event_data['body'])
    print(data)
    return _create_response(cognito.auth(data))


def register(event, context):
    print(event)
    event_data = json.loads(json.dumps(event))
    data = json.loads(event_data['body'])
    print(data)
    return _create_response(cognito.register(data))

def _create_response(response):
    print(response)
    status, body = response
    result = {
        'statusCode': status,
        'body': json.dumps(body),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
         }
    }
    print(result)
    return result
