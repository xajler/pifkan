import os
import boto3
import time
from datetime import datetime, timedelta

dynamodb = boto3.resource("dynamodb")
d_table = dynamodb.Table(os.environ.get('DRAWS_DYNAMODB_TABLE'))


def handler(event, context):
    try:
        print('Data in: {0}'.format(event))
        draw_id = event['id']
        _create_current_draw(draw_id)
        return event
    except Exception as e:
        print(e)
        raise


def _create_current_draw(draw_id):
    id = draw_id + 1
    print(id)
    draw_at = datetime.now()
    print(draw_at)
    draw_at += timedelta(days=1)
    print(draw_at)
    timestamp = int(time.mktime(draw_at.timetuple()))
    item = {'id': id, 'drawAt': timestamp, 'status': 'current'}
    d_table.put_item(Item=item)
