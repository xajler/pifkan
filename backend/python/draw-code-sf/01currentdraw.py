import os
import boto3
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
d_table = dynamodb.Table(os.environ.get('DRAWS_DYNAMODB_TABLE'))


def handler(event, context):
    try:
        draw = _get_current_draw()
        if draw is None:
            raise Exception('There is no current draw.')
        data = {'draw': draw}
        print('Data out: {0}'.format(data))
        return data
    except Exception as e:
        print(e)
        raise


def _get_current_draw():
    result = d_table.scan(FilterExpression=Attr('status').eq('current'))
    print(result)

    if len(result['Items']) > 0:
        print(result['Items'])
        item = result['Items'][0]
        return {
            'id': int(item['id']),
            'status': item['status'],
            'drawAt': int(item['drawAt'])
        }
    else:
        print('No items')
        return None
