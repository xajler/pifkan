import os
import boto3
import time
from datetime import datetime
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
d_table = dynamodb.Table(os.environ.get('DRAWS_DYNAMODB_TABLE'))


def handler(event, context):
    try:
        print('Data in: {0}'.format(event))
        draw = event['draw']
        drawn_code = event['drawn_code']
        draw_codes = event['draw_codes']
        res_lst = list(filter(lambda x: x['code'] == drawn_code, draw_codes))
        if len(res_lst) > 0:
            timestamp = int(time.mktime(datetime.now().timetuple()))
            result = res_lst[0]
            _update_draw(draw, result, timestamp)
        else:
            raise Exception('Drawn code is not found in table drawn codes. Exiting...')
        data = {'id': draw['id']}
        print('Data out: {0}'.format(data))
        return data
    except Exception as e:
        print(e)
        raise


def _update_draw(draw, drawn_code, timestamp):
    draw['drawnCode'] = drawn_code['code']
    draw['drawnUser'] = drawn_code['submittedBy']
    draw['drawnAt'] = timestamp
    draw['status'] = 'done'
    print(draw)
    d_table.put_item(Item=draw)
