import os
import boto3
from datetime import datetime
from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(os.environ.get('DYNAMODB_TABLE'))


def handler(event, context):
    try:
        print('Data in: {0}'.format(event))
        draw = event['draw']
        draw_id = draw['id']
        draw_codes = _current_draw_codes(draw_id)
        if len(draw_codes) == 0:
            raise Exception('Found no submitted codes for current draw. Exiting...')
        data = {'draw': draw, 'draw_codes': draw_codes}
        print('Data out: {0}'.format(data))
        return data
    except Exception as e:
        print(e)
        raise


def _current_draw_codes(draw_id):
    items = _get_current_draw_codes(draw_id)
    result = []
    if items is None : return result
    for item in items:
        obj = {
          'code': item['code'],
          'submittedBy': item['submittedBy'],
          'submittedAt': datetime.fromtimestamp(item['submittedAt']).isoformat()
          }
        result.append(obj)
    print(result)
    return result


def _get_current_draw_codes(draw_id):
    result = table.scan(
        FilterExpression=Attr('drawId').eq(draw_id)
    )
    print(result)

    if len(result['Items']) > 0:
        print(result['Items'])
        return result['Items']
    else:
        print('No current draw codes items')
        return None
