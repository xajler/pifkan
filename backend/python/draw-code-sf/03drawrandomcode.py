import random

def handler(event, context):
    try:
        print('Data in: {0}'.format(event))
        draw_codes = event['draw_codes']
        codes = list(map(lambda x: x['code'], draw_codes))
        drawn_code = _get_random_code(codes)
        if drawn_code is None:
            raise Exception('No code is drawn. Exiting...')
        data = {
            'draw': event['draw'],
            'draw_codes': draw_codes,
            'drawn_code': drawn_code
        }
        print('Data out: {0}'.format(data))
        return data
    except Exception as e:
        print(e)
        raise

def _get_random_code(codes):
    if len(codes) > 0:
        draw = random.choice(codes)
        print(draw)
        return draw
    else:
        print('No codes...')
        return None
