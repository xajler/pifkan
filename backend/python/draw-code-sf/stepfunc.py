import json
import os
import boto3
from auth import validate_admin

client = boto3.client('stepfunctions')

# step funcs
# - GetCurrentDraw
# - GetDrawSubmittedCodes
# - DrawRandomCode
# - UpdateDraw
# - CreateNewDraw

def execute(event, context):
    event_data = json.loads(json.dumps(event))
    auth_header = event_data['headers']['Authorization']
    status, body = validate_admin(auth_header)
    if status > 200: return _create_response((status, body))

    client.start_execution(
        stateMachineArn=os.environ.get('STATE_MACHINE_ARN'),
        input='{"status": "started"}'
    )

    response = (200, 'Execution started!')
    return _create_response(response)


def _create_response(response):
    print(response)
    status, body = response
    result = {
        'statusCode': status,
        'body': json.dumps(body),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
         }
    }
    print(result)
    return result
