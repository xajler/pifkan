import json
import os
from auth import validate_admin, validate_user
from helper import submit_code_for_draw, get_draw_codes, get_drawn_random_code

def submit_draw_code(event, context):
    event_data = json.loads(json.dumps(event))
    auth_header = event_data['headers']['Authorization']
    status, body = validate_user(auth_header)
    if status > 200: return _create_response((status, body))
    if body['group'] == 'admin':
        return _create_response(403, 'Admin is not allowed to submit code for draw')
    data = json.loads(event_data['body'])
    print(data)
    code = data['code']
    print(code)
    return _create_response(submit_code_for_draw(code, body['username']))


def current_draw_codes(event, context):
    event_data = json.loads(json.dumps(event))
    auth_header = event_data['headers']['Authorization']
    status, body = validate_admin(auth_header)
    if status > 200: return _create_response((status, body))
    return _create_response(get_draw_codes())


def draw_random_code(event, context):
    event_data = json.loads(json.dumps(event))
    auth_header = event_data['headers']['Authorization']
    status, body = validate_admin(auth_header)
    if status > 200: return _create_response((status, body))
    response = get_drawn_random_code()
    print(response)
    return _create_response(response)


def _create_response(response):
    print(response)
    status, body = response
    result = {
        'statusCode': status,
        'body': json.dumps(body),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
         }
    }
    print(result)
    return result
