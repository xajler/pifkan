import boto3
import os
from botocore.exceptions import ClientError
from jose import jwt

client = boto3.client('cognito-idp')
user_pool_id = os.environ.get('COGNITO_POOL_ID')
client_id = os.environ.get('COGNITO_CLIENT_ID')


def validate_user(auth):
    token = auth.split()[1]
    print(token)
    status, body = _get_user_role(token)
    return (status, body)


def validate_admin(auth):
    token = auth.split()[1]
    print(token)
    status, body = _get_user_role(token)
    if status == 200:
        if body['group'] == 'admin':
            return (status, body)
        else:
            msg = 'User role is not authorized to view this resource'
            return(403, {'error': msg})
    else:
        return (status, body)


def _get_user_role(token):
    print(token)
    try:
        response = client.admin_initiate_auth(
                UserPoolId=user_pool_id,
                ClientId=client_id,
                AuthFlow='REFRESH_TOKEN',
                AuthParameters={'REFRESH_TOKEN': token}
        )
        print(response)

        id_token = response['AuthenticationResult']['IdToken']
        print(id_token)
        token_data = jwt.get_unverified_claims(id_token)
        print(token_data)
        username = token_data['cognito:username']
        print(username)
        group = token_data['cognito:groups'][0]
        print(group)
        return (200, {'username': username, 'group': group})
    except ClientError as e:
        print(e.response)
        if e.response['Error']['Code'] == 'UserNotFoundException':
            return (403, {'error': 'User does not exists'})
        return (500, {'error': 'Unable to authenticate user, server error'})


        # usr_resp = client.admin_get_user(
        #        UserPoolId=user_pool_id,
        #        Username=username
        # )
        # print(usr_resp)
