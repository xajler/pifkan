import os
import boto3
import time
import random
from datetime import datetime, timedelta
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(os.environ.get('DYNAMODB_TABLE'))
d_table = dynamodb.Table(os.environ.get('DRAWS_DYNAMODB_TABLE'))
gc_table = dynamodb.Table(os.environ.get('GEN_CODES_DYNAMODB_TABLE'))


def get_draw_codes():
    try:
        draw = _get_current_draw()
        if draw is None: return (404, {'error': 'No current draw'})
        result = _current_draw_codes(draw['id'])
        return (200, result)
    except ClientError as e:
        print(e.response)
        return (500, {'error': 'Unable to get codes for draw, server error'})

def submit_code_for_draw(code, username):
    try:
        print(username)
        status, body = _get_code(code)
        if status > 200: return (status, body)
        draw = _get_current_draw()
        if draw is None: return (404, {'error': 'No current draw'})
        timestamp = int(time.mktime(datetime.now().timetuple()))
        print(timestamp)
        item = {
                'code': code,
                'submittedAt': timestamp,
                'drawId': draw['id'],
                'submittedBy': username
                }
        print(item)
        table.put_item(Item=item)
        _del_code_from_gencodes(code)
        return (201, {'success': 'Code {0} is submitted.'.format(code)})
    except ClientError as e:
        print(e.response)
        return (500, {'error': 'Unable to save code for draw, server error'})


def get_drawn_random_code():
    try:
        draw = _get_current_draw()
        if draw is None: return (404, {'error': 'No current draw'})
        draw_codes = _current_draw_codes(draw['id'])
        if len(draw_codes) == 0:
            return (404, {'error':'Found no submitted codes for current draw'})
        codes = list(map(lambda x: x['code'], draw_codes))
        print(codes)
        drawn_code = _get_random_code(codes)
        res_lst = list(filter(lambda x: x['code'] is drawn_code, draw_codes))
        if len(res_lst) > 0:
            result = res_lst[0]
            _update_draw(draw, result)
            _create_current_draw(draw['id'])
            return (201, result)
        else:
            return (404, {'error': 'Drawn code is not found in current draw codes.'})
    except ClientError as e:
        print(e.response)
        return (500, {'error': 'Unable draw a code, server error'})


def _update_draw(draw, drawn_code):
    timestamp = int(time.mktime(datetime.now().timetuple()))
    draw['drawnCode'] = drawn_code['code']
    draw['drawnUser'] = drawn_code['submittedBy']
    draw['drawnAt'] = timestamp
    draw['status'] = 'done'
    print(draw)
    d_table.put_item(Item=draw)


def _create_current_draw(draw_id):
    id = draw_id + 1
    print(id)
    draw_at = datetime.now()
    print(draw_at)
    draw_at += timedelta(days=1)
    print(draw_at)
    timestamp = int(time.mktime(draw_at.timetuple()))
    item = {'id': id, 'drawAt': timestamp, 'status': 'current'}
    d_table.put_item(Item=item)


def _get_random_code(codes):
    result = random.choice(codes)
    print(result)
    return result


def _current_draw_codes(draw_id):
    items = _get_current_draw_codes(draw_id)
    result = []
    if items is None : return result
    for item in items:
        submitted_at = datetime.fromtimestamp(item['submittedAt']).isoformat()
        submitted_at += 'Z'
        obj = {
          'code': item['code'],
          'submittedBy': item['submittedBy'],
          'submittedAt': submitted_at
          }
        result.append(obj)
    print(result)
    return result


def _del_code_from_gencodes(code):
    gc_table.delete_item(Key={'code': code})


def _get_current_draw():
    result = d_table.scan(FilterExpression=Attr('status').eq('current'))
    print(result)

    if len(result['Items']) > 0:
        print(result['Items'])
        return result['Items'][0]
    else:
        print('No current draw found!')
        return None


def _get_current_draw_codes(draw_id):
    result = table.scan(
        FilterExpression=Attr('drawId').eq(draw_id)
    )
    print(result)

    if len(result['Items']) > 0:
        print(result['Items'])
        return result['Items']
    else:
        print('No codes found for current draw')
        return None


def _get_code(code):
    print(os.environ.get('GEN_CODES_DYNAMODB_TABLE'))
    try:
        result = gc_table.get_item(Key={'code': code})
        print(result)
        result['Item']
        return (200, {'success': 'Code {0} found'.format(code)})
    except KeyError:
        return (404, {'error': 'Code {0} is not found'.format(code)})
    except ClientError as e:
        print('Unable to retrive code')
        print(e)
        return (500, {'error': 'Unable to retrieve code, server error'})
