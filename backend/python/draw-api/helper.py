import os
import boto3
import time
from datetime import datetime
import iso8601
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError

dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table(os.environ.get('DYNAMODB_TABLE'))


def create(draw_at):
    # TODO: test draw_at is > date.now + 5 mins
    id = 1
    current = _get_current()
    try:
        if current:
            print(current)
            id = current['id'] + 1
            _update_status(current)

        print(id)
        draw_date = iso8601.parse_date(draw_at)
        timestamp = int(time.mktime(draw_date.timetuple()))
        print('{0} - {1}'.format(draw_at, timestamp))
        item = {'id': int(id), 'drawAt': timestamp, 'status': 'current'}
        table.put_item(Item=item)
        return (201, {})
    except ClientError as e:
        print(e)
        return (500, {'error': 'Unable to save draw, server error'})

def get_current():
    try:
        currentItem = _get_current()
        if currentItem:
            print(currentItem)
            draw_at = datetime.fromtimestamp(currentItem['drawAt']).isoformat()
            draw_at += 'Z'
            currentItem['id'] = int(currentItem['id'])
            currentItem['drawAt'] = draw_at
            return (200, currentItem)
        else:
            return (404, {'error': 'No current or any draw'})
    except ClientError as e:
        print('Unable to retrive code')
        print(e)
        return (500, {'error': 'Unable to get current draw, server error'})


def get_all():
    try:
        result = table.scan(FilterExpression=Attr('status').eq('done'))
        print(result)
        draws = []

        if 'Items' in result:
            for i in result['Items']:
                draw_at = datetime.fromtimestamp(i['drawAt']).isoformat()
                draw_at += 'Z'
                i['id'] = int(i['id'])
                i['drawAt'] = draw_at
                if 'drawnAt' in i:
                    print(type(i['drawnAt']))
                    drawn_at = int(i['drawnAt'])
                    drawn_at_date = datetime.fromtimestamp(drawn_at).isoformat()
                    drawn_at_date += 'Z'
                    i['drawnAt'] =  drawn_at_date
                draws.append(i)
            print(draws)
            return (200, draws)
        else:
            return (200, draws)
    except ClientError as e:
        print(e)
        return (500, {'error': 'Unable to retrieve draws, server error'})


def _get_current():
    result = table.scan(
        FilterExpression=Attr('status').eq('current')
    )
    print(result)

    if len(result['Items']) > 0:
        print(result['Items'])
        return result['Items'][0]
    else:
        print('No items')
        return None


def _update_status(item):
    item = {'id': item['id'], 'drawAt': item['drawAt'], 'status': 'done'}
    table.put_item(Item=item)
