import json
from auth import validate_admin, validate_user
from helper import get_current, get_all, create


def draw(event, context):
    print(event)
    event_data = json.loads(json.dumps(event))
    print(event_data['httpMethod'])
    auth_header = event_data['headers']['Authorization']

    if event_data['httpMethod'] == 'POST':
        print('HttpMethod: POST')
        status, body = validate_admin(auth_header)
        if status > 200: return _create_response((status, body))

        data = json.loads(event_data['body'])
        print(data)
        return _create_response(create(data['drawAt']))
    else:
        print('HttpMethod: GET')
        status, body = validate_user(auth_header)
        if status > 200: return _create_response((status, body))

        if 'current' in event_data['resource']:
            return _create_response(get_current())
        else:
            return _create_response(get_all())


def _create_response(response):
    print(response)
    status, body = response
    result = {
        'statusCode': status,
        'body': json.dumps(body),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True
         }
    }
    print(result)
    return result
