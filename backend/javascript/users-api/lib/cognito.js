const aws = require('aws-sdk');
const to = require('await-to-js').default;

const client = new aws.CognitoIdentityServiceProvider();
const genericPass = "%xg123$Yn";
const fstTimePass = "Abc$1234";
const userPoolId = process.env.COGNITO_POOL_ID;
const clientId = process.env.COGNITO_CLIENT_ID;

const _addUserToGroup = async(username) => {
    let params = {
        UserPoolId: userPoolId,
        Username: username,
        GroupName: 'user'
    };
    return await to(client.adminAddUserToGroup(params).promise());
};

exports.auth = async(data) => {
    console.log(data);
    let username = data.email;
    let paramsInit = {
        UserPoolId: userPoolId,
        ClientId: clientId,
        AuthFlow: 'ADMIN_NO_SRP_AUTH',
        AuthParameters: {
            'USERNAME': username,
            'PASSWORD': genericPass
        }
    };
    let [errInit, respInit] = await to(client.adminInitiateAuth(paramsInit).promise());
    if (errInit) {
        console.dir(JSON.stringify(errInit));
        if(errInit.code === 'UserNotFoundException') {
            return [404, { 'error': 'User does not exists' }];
        }
        throw errInit;
    }
    console.dir(JSON.stringify(respInit));
    let refreshToken = respInit.AuthenticationResult.RefreshToken;
    let paramsGrp = {
        UserPoolId: userPoolId,
        Username: username,
        Limit: 1
    };
    let [errGrp, respGrp] = await to(client.adminListGroupsForUser(paramsGrp).promise());
    if (errGrp) { console.dir(JSON.stringify(errGrp)); throw errGrp; }
    console.dir(JSON.stringify(respGrp));
    let role = respGrp.Groups[0].GroupName;
    return [200, { 'role': role, 'refreshToken': refreshToken }];
};

exports.register = async(data) => {
    console.log(data);
    let username = data.email;
    var paramsCreate = {
        UserPoolId: userPoolId,
        Username: username,
        TemporaryPassword: fstTimePass,
        MessageAction: 'SUPPRESS',
        UserAttributes: [
            {
                Name: 'email',
                Value: username
            }
        ]
    };
    let [errCreate, respCreate] = await to(client.adminCreateUser(paramsCreate).promise());
    if (errCreate) {
        if(errCreate.code === 'UsernameExistsException') {
            console.dir(JSON.stringify(errCreate));
            return [409, { 'error': 'Email is used, please try another one' }];
        }

    }
    console.dir(JSON.stringify(respCreate));
    let paramsInit = {
        UserPoolId: userPoolId,
        ClientId: clientId,
        AuthFlow: 'ADMIN_NO_SRP_AUTH',
        AuthParameters: {
            'USERNAME': username,
            'PASSWORD': fstTimePass
        }
    };
    let [errInit, respInit] = await to(client.adminInitiateAuth(paramsInit).promise());
    if (errInit) { console.dir(JSON.stringify(errInit)); throw errInit; }
    console.dir(JSON.stringify(respInit));
    let params = {
        ClientId: clientId,
        ChallengeName: 'NEW_PASSWORD_REQUIRED',
        Session: respInit.Session,
        ChallengeResponses: {
            'USERNAME': username,
            'NEW_PASSWORD': genericPass
        }
    };
    let [err, resp] = await to(client.respondToAuthChallenge(params).promise());
    if (err) { console.dir(JSON.stringify(err)); throw err; }
    console.dir(JSON.stringify(resp));
    let [errGrp, respGrp] = await to(_addUserToGroup(username));
    if (errGrp) { console.dir(JSON.stringify(errGrp)); throw errGrp; }
    console.log(respGrp);
    return [201, {}];
};
