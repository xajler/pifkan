const cognito = require('./lib/cognito');

const _createResponse = (response) => {
    console.log(response);
    let [status, body] = response;
    return {
        statusCode: status,
        body: JSON.stringify(body)
    };
};

module.exports.auth = async(event, context, callback) => {
    try {
        let body = event.body;
        let data = JSON.parse(body);
        console.log(data);
        let response = await cognito.auth(data);
        callback(null, _createResponse(response));
    } catch (err) {
        console.dir(err);
        throw new Error(err);
    }
};

module.exports.register = async(event, context, callback) => {
    try {
        let body = event.body;
        let data = JSON.parse(body);
        console.log(data);
        let response = await cognito.register(data);
        callback(null, _createResponse(response));
    } catch (err) {
        console.dir(err);
        throw new Error(err);
    }
};

