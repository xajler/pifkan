const auth = require('./lib/auth');
const helper = require('./lib/helper');

const _createResponse = (response) => {
    console.log(response);
    let [status, body] = response;
    return {
        statusCode: status,
        body: JSON.stringify(body)
    };
};

exports.submitDrawCode =  async (event, context, callback) => {
    console.dir(JSON.stringify(event));
    let authHeader = event.headers['Authorization'];
    let data = JSON.parse(event.body);
    let [status, body] = await auth.validateUser(authHeader);
    if (status > 200) return _createResponse([status, body]);
    if (body.group === 'admin')
        return _createResponse([403, { error: 'Admin is not allowed to submit code for draw'}]);
    let username = body.username;
    console.log(`username: ${username}`);
    let code = data.code;
    console.log(`code: ${code}`);
    let result = await helper.submitCodeForDraw(code, username);
    return _createResponse(result);
};

exports.currentDrawCodes = async (event, context, callback) => {
    console.dir(JSON.stringify(event));
    let authHeader = event.headers['Authorization'];
    let [status, body] = await auth.validateAdmin(authHeader);
    if (status > 200) return _createResponse([status, body]);
    let result = await helper.getDrawCodes();
    return _createResponse(result);
};

exports.drawRandomCode = async (event, context, callback) => {
    console.dir(JSON.stringify(event));
    let authHeader = event.headers['Authorization'];
    let [status, body] = await auth.validateAdmin(authHeader);
    if (status > 200) return _createResponse([status, body]);
    let result = await helper.getDrawnRandomCode();
    return _createResponse(result);
};
