const aws = require('aws-sdk');
const to = require('await-to-js').default;

const dynamo = new aws.DynamoDB();
const table = process.env.DYNAMODB_TABLE;
const dTable = process.env.DRAWS_DYNAMODB_TABLE;
const gcTable = process.env.GEN_CODES_DYNAMODB_TABLE;

// PRIVATE
const _getCurrentDraw = async () => {
    let params = {
        ExpressionAttributeValues: { ":status": { S: "current" } },
        ExpressionAttributeNames: { "#status": "status" },
        FilterExpression: "#status = :status",
        TableName: dTable
    };
    let result = await dynamo.scan(params).promise();
    if (result.Items.length > 0) {
        console.dir(JSON.stringify(result.Items));
        let draw = result.Items[0];
        return {
            id: draw.id.N,
            status: draw.status.S,
            drawAt: draw.drawAt.N
        }
    } else {
        console.log('No current draw found!');
        return null;
    }
};

const _getCurrentDrawCodes = async (id) => {
    let params = {
        ExpressionAttributeValues: { ":id": { N: id } },
        FilterExpression: "drawId = :id",
        TableName: table
    };
    let result = await dynamo.scan(params).promise();
    if (result.Items.length > 0) {
        console.dir(JSON.stringify(result.Items));
        let items = [];
        result.Items.forEach(function(item) {
            console.log(`item: ${JSON.stringify(item)}`);
            let timestamp = item.submittedAt.S * 1000
            let obj = {
                code: item.code.S,
                submittedBy: item.submittedBy.S,
                submittedAt: new Date(timestamp).toISOString()
            };
            console.log(`obj: ${JSON.stringify(obj)}`);
            items.push(obj);
        });
        console.dir(JSON.stringify(items));
        return items;
    } else {
        console.log('No codes found for current draw');
        return [];
    }
};

const _getCode = async (code) => {
    let params = { Key: { "code": { S: code } }, TableName: gcTable };
    let [err, result] = await to(dynamo.getItem(params).promise());
    if (err) {
        console.dir(JSON.stringify(err));
        // TODO: Check for KeyError, from err logged.
        // if key error:
        // return [404, { error: `Code '${code}' is found` }];
        return [500, { error: 'Unable to retrieve code, server error' }];
    }
    console.dir(JSON.stringify(result));
    return [200, { success: `Code '${code}' is found` }];
};

const _delCodeFromGenCodes = async (code) => {
    let params = { Key: { "code": { S: code } }, TableName: gcTable };
    let [err, ] = await to(dynamo.deleteItem(params).promise());
    if (err) {
        console.dir(JSON.stringify(err));
        return [500, { error: 'Unable to remove code for gen codes' }];
    }
    return [200, { success: 'done' }];
};

const _getRandomCode = (codes) => {
    let result = codes[(Math.random() * codes.length) | 0];
    console.dir(JSON.stringify(result));
    return result;
};

const _updateDraw = async(drawId, drawAt, drawnCodeObj) => {
    let timestamp = Math.round(Date.now() / 1000)
    let draw = {
        id: { N: drawId },
        drawAt: { N: drawAt },
        status: { S: 'done' },
        drawnCode: { S: drawnCodeObj.code },
        drawnUser: { S: drawnCodeObj.submittedBy },
        drawnAt: { S: timestamp.toString() }
    };
    console.dir(JSON.stringify(draw));
    let params = { Item: draw, TableName: dTable };
    let [err, ] = await to(dynamo.putItem(params).promise());
    if (err) {
        console.dir(JSON.stringify(err));
        return [500, { error: 'Unable to update draw' }];
    }
    return [200, {}];
};

const _createCurrentDraw = async (drawId) => {
    let id = drawId + 1;
    console.log(id);
    let timestamp = (Math.round(Date.now() / 1000)) + 86400; // now + 1 day in sec
    let item = {
        id: { N: id },
        drawAt: timestamp,
        status: { S: 'current' }
    };
    let params = { Item: item, TableName: dTable };
    let [err, ] = await to(dynamo.putItem(params).promise());
    if (err) {
        console.dir(JSON.stringify(err));
        return [500, { error: 'Unable to create current draw' }];
    }
    return [200, {}];
};

// EXPORTS
exports.getDrawCodes = async () => {
    try {
        let draw = await _getCurrentDraw();
        console.dir(`draw: ${JSON.stringify(draw)}`);
        if (draw === null) return [404, { error: 'No current draw found' }];
        let result = await _getCurrentDrawCodes(draw.id);
        console.dir(JSON.stringify(result));
        return [200, result];
    } catch (err) {
        console.dir(JSON.stringify(err));
        return [500, { error: 'Unable to get codes for draw, server error' }];
    }
};

exports.submitCodeForDraw = async (code, username) => {
    let [status, body] = await _getCode(code);
    if (status > 200) return [status, body];
    let draw = await _getCurrentDraw();
    if (draw === null) return [404, { error: 'No current draw found' }];
    let timestamp = Math.round(Date.now() / 1000);
    let item = {
        "code": { S: code },
        "submittedAt": { S: timestamp.toString() },
        "drawId": { N: draw.id },
        "submittedBy": { S: username }
    };
    console.dir(JSON.stringify(item));
    let params = {
        Item: item,
        TableName: table
    };
    let [err, ] = await to(dynamo.putItem(params).promise());
    if (err) {
        console.dir(JSON.stringify(err));
        return [500, { error: 'Unable to submit code to table, server error' }]; }
    [status, body] = await _delCodeFromGenCodes(code);
    if (status === 500) return [status, body];
    return [201, { success: `Code ${code} is submitted for draw.` }];
};

exports.getDrawnRandomCode = async () => {
    let draw = await _getCurrentDraw();
    if (draw === null) return [404, { error: 'No current draw found' }];
    let drawCodes = await _getCurrentDrawCodes(draw.id);
    if (drawCodes.length === 0)
        return [404, { error: 'Found no submitted codes for current draw' }];
    let codes = drawCodes.map(dc => { return dc.code; });
    console.log(JSON.stringify(codes));
    let drawnCode = _getRandomCode(codes);
    let result = drawCodes.find((dc) => { return dc.code === drawnCode; });
    console.dir(JSON.stringify(result));
    if (result) {
        let [status, body] = await _updateDraw(draw.id, draw.drawAt, result);
        if (status === 500) return [status, body];
        [status, body] = await _createCurrentDraw(draw.id);
        if (status === 500) return [status, body];
        console.dir(JSON.stringify(result));
        return [201, result];
    } else {
        return [404, { error: 'Drawn code is not found in current draw codes' }];
    }
};
