const aws = require('aws-sdk');
const to = require('await-to-js').default;
const jwt = require('jsonwebtoken');

const client = new aws.CognitoIdentityServiceProvider();
const userPoolId = process.env.COGNITO_POOL_ID;
const clientId = process.env.COGNITO_CLIENT_ID;

const _getUserRole = async(token) => {
    console.log(token);
    let params = {
        UserPoolId: userPoolId,
        ClientId: clientId,
        AuthFlow: 'REFRESH_TOKEN',
        AuthParameters: { 'REFRESH_TOKEN': token }
    };
    let [err, resp] = await to(client.adminInitiateAuth(params).promise());
    if (err) {
        console.dir(JSON.stringify(err));
        if(err.code === 'UserNotFoundException') {
            console.dir(JSON.stringify(err.code));
            return [404, { 'error': 'User does not exists' }];
        }
    }
    console.dir(JSON.stringify(resp));
    let idToken = resp.AuthenticationResult.IdToken;
    let tokenData = jwt.decode(idToken);
    let result = {
        username: tokenData['cognito:username'],
        group: tokenData['cognito:groups'][0]
    };
    return [200, result];
};

exports.validateUser = async(auth) => {
    let token = auth.split(' ')[1];
    console.log(token);
    let result = await _getUserRole(token);
    return result;
};

exports.validateAdmin = async(auth) => {
    let token = auth.split(' ')[1];
    console.log(token);
    let result = await _getUserRole(token);
    let [status, body] = result;
    if (status === 200) {
        if (body.group === 'admin') {
            return result;
        } else {
            let msg = "User role is not authorized to view thi resource.";
            return [403, { error: msg }];
        }
    } else {
        return result;
    }
};
